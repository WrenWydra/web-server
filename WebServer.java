//import necessary items for server function
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class WebServer {
    private static final String FILE_DEFAULT = "index.html";
    // in an ideal world these would never be hardcoded -- fix!!
    private static final String FILE_NOT_FOUND = "<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\">\n" +
            "    <title>NOT FOUND</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "Error 404: This server could not find the requested URL.\n" +
            "</body>\n" +
            "</html>";
    private static final String FILE_NOT_IMPLEMENTED = "<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\">\n" +
            "    <title>NOT IMPLEMENTED</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "Error 501: This server has not implemented the input method.\n" +
            "</body>\n" +
            "</html>";
    private static final String FILE_BAD_REQUEST = "<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\">\n" +
            "    <title>BAD REQUEST</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "Error 400: This server could not find the specified URL.\n" +
            "</body>\n" +
            "</html>";
    
    private static final String FILE_MODIFIED = "<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\">\n" +
            "    <title>NOT MODIFIED SINCE</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "Error 304: This server could not return specified file due to date constraints.\n" +
            "</body>\n" +
            "</html>";
    
    private static File directory;

    /**
     * represents the request from the client
     */
    static class Request {
        private String method = "Unknown";
        private String path = null;
        private Map<String, String> headers = new HashMap<>();

        Request(InputStream inputStream) {
            try {
                BufferedReader inFromClient = new BufferedReader(new InputStreamReader(inputStream));
                String requestLine = inFromClient.readLine();
                System.out.println("request:" + requestLine);
                StringTokenizer parseInput = new StringTokenizer(requestLine);
                this.method = parseInput.nextToken().toUpperCase();
                this.path = parseInput.nextToken().toLowerCase();

                if (path.endsWith("/")) {
                    path += FILE_DEFAULT;
                }

                // read headers from input and add to Map.
                String requestPart = inFromClient.readLine();
                while (requestPart != null && requestPart.length() > 0) {
                    int colonPos = requestPart.indexOf(':');
                    String headerKey = requestPart.substring(0, colonPos);
                    String headerValue = requestPart.substring(colonPos + 1).trim();
                    headers.put(headerKey, headerValue);
                    requestPart = inFromClient.readLine();
                }
            } catch (Exception e) {
                this.method = "Unknown";
                this.path = "/" + FILE_DEFAULT;
                // should probably not throw an exception if this is in the main Thread
                // since that would cause the web server to exit
                // if this is called in a thread, then throwing an exception is ok
                System.err.println("error getting client request.");
            }    
        }

        String getMethod() {
            return method;
        }

        String getPath() {
            return path;
        }

        String getHeader(String header) {
            return headers.get(header);
        }
    }

    /**
     * all methods extend this command: GET, HEAD, etc.
     */
    static abstract class AbstractMethod {
        protected Request request;
        protected Socket socket;
        protected DataOutputStream dataOut = null;

        AbstractMethod(Request request, Socket socket) {
            this.request = request;
            this.socket = socket;

            try {
                dataOut = new DataOutputStream(socket.getOutputStream());
            } catch (Exception e) {
                
            }
        }

        abstract void doCommand() throws Exception;
    }

    static class GetMethod extends AbstractMethod {
        GetMethod(Request request, Socket socket) {
            super(request, socket);
        }

        @Override
        public void doCommand() throws Exception {
            dataOut = new DataOutputStream(socket.getOutputStream());

            File fileRequested = new File(directory, request.getPath());
            
            int fileLength = (int) fileRequested.length();
            String fileContent = getContentType(fileRequested.getName());
            byte[] fileData = readFileData(fileRequested, fileLength);
            
            dataOut.writeBytes("HTTP/1.1 200 OK\r\n");
            dataOut.writeBytes("Last-Modified: " + fileRequested.lastModified() + "\r\n");
            basicHeader(fileContent, fileLength, dataOut);
            dataOut.flush();
            dataOut.write(fileData, 0, fileLength);
            dataOut.flush();
            dataOut.close();
        }
    }

    static class HeadMethod extends AbstractMethod {
        HeadMethod(Request request, Socket socket) {
            super(request, socket);
        }

        @Override
        public void doCommand() throws Exception {
            dataOut = new DataOutputStream(socket.getOutputStream());
            File fileRequested = new File(directory, request.getPath());
            int fileLength = (int) fileRequested.length();
            String fileContent = getContentType(fileRequested.getName());

            dataOut.writeBytes("HTTP/1.1 200 OK\r\n");
            dataOut.writeBytes("Last-Modified: " + fileRequested.lastModified() + "\r\n");
            basicHeader(fileContent, fileLength, dataOut);
            dataOut.flush();
            dataOut.close();
        }
    }

    static class UnsupportedMethod extends AbstractMethod {
        UnsupportedMethod(Request request, Socket socket) {
            super(request, socket);
        }

        @Override
        public void doCommand() throws Exception {
            int dataLength = FILE_NOT_IMPLEMENTED.length();

            // this doesn't do new lines -- might want to add those
            dataOut.writeBytes("HTTP/1.1 501 Not Implemented");
            dataOut.writeBytes("Last-Modified: " + new Date());
            dataOut.writeBytes("Date: " + new Date());
            dataOut.writeBytes("Server: Java12 HTTP Server");
            dataOut.writeBytes("Content-Length: " + dataLength);
            dataOut.writeBytes("Content-Type: text/html\r\n\r\n");
            dataOut.writeBytes(FILE_NOT_IMPLEMENTED);
            dataOut.flush();
            dataOut.close();
        }
    }

    static class SocketHandler implements Runnable {
        private Socket socket;

        SocketHandler(Socket socket) {
            this.socket = socket;
        }

        public void run() {
            try {
                Request request = new Request(socket.getInputStream());
                File targetResource = new File(directory, request.getPath());
                // Handles the case of when we can't find file requested -- 404 case
                if (!validUrl(request.getPath())) {
                    System.out.println("bad url");
                    badRequest(request, socket);
                } else if (!targetResource.exists()) {
                    System.out.println("file not found");
                    fileNotFound(request, socket);
                } else if (!isFileModifiedSince(request)) {
                    System.out.println("file not modified");
                    notModifiedSince(request, socket);
                } else {
                    AbstractMethod method = new UnsupportedMethod(request, socket);
                    if (request.getMethod().equals("GET")) {
                        method = new GetMethod(request, socket);
                    } else if (request.getMethod().equals("HEAD")) {
                        method = new HeadMethod(request, socket);
                    }

                    method.doCommand();
                }
            } catch (Exception e) {
                System.err.println("Unable to process request");
                e.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (Exception e) {
                }
            }
        }

        private boolean validUrl(String path) {
            try {
                new URL("http://localhost/" + path).toURI();
                return true;
            } catch (Exception e) {
            }
            return false;
        }
    }

    public static void main(String[] args) throws Exception {
        int port = Integer.parseInt(args[0]);
        directory = new File(args[1]);
        
        //Create serverSocket for incoming request
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("Server initiated, listening for connections... ");

        while(true) {
            // Wait for incoming connection request
            Socket socket = serverSocket.accept();
            System.out.println("Connection opened to client.");
            new Thread(new SocketHandler(socket)).start();
            System.out.println("handling request in new thread.");
        }
    }

    private static byte[] readFileData(File file, int fileLength) throws IOException {
        FileInputStream in;
        byte[] fileData = new byte[fileLength];
        in = new FileInputStream(file);
        in.read(fileData);

        if (in != null) {
            in.close();
        }

        return fileData;
    }

    private static String getContentType(String fileRequested) {
        if (fileRequested.endsWith(".htm") || fileRequested.endsWith(".html")) {
            return "text/html";
        }
        else {
            return "text/plain";
        }
    }

    private static void fileNotFound(Request request, Socket socket) throws Exception {
        returnError("HTTP/1.1 404 File Not Found", FILE_NOT_FOUND, socket);
    }

    private static void badRequest(Request request, Socket socket) throws IOException {
        returnError("HTTP/1.1 400 Bad Request", FILE_BAD_REQUEST, socket);
    }

    private static void notModifiedSince(Request request, Socket socket) throws IOException {
        returnError("HTTP/1.1 304 Not Modified", FILE_MODIFIED, socket);
    }

    private static void returnError(String status, String content, Socket socket) throws IOException {
        DataOutputStream dataOut = null;
        try {
            dataOut = new DataOutputStream(socket.getOutputStream());
            dataOut.writeBytes(status + "\r\n");
            basicHeader("text/html", content.getBytes().length, dataOut);
            dataOut.writeBytes(content);
            dataOut.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (dataOut != null) {
                dataOut.close();
            }
        }
    }

    private static void basicHeader(String fileContent, int strLength, DataOutputStream dataOut) throws IOException {
        dataOut.writeBytes("Date: " + new Date() + "\r\n");
        dataOut.writeBytes("Server: Java12 HTTP Server\r\n");
        dataOut.writeBytes("Content-Length: " + strLength + "\r\n");
        dataOut.writeBytes("Content-Type: " + fileContent + "\r\n\r\n");
        dataOut.flush();
    }

    private static final SimpleDateFormat DATE_FORMAT =
            new SimpleDateFormat("EEE, d MMM yyyy hh:mm:ss zzz");
    private static boolean isFileModifiedSince(Request request) throws Exception {
        String ifModifiedSinceString = request.getHeader("If-Modified-Since");
        if (ifModifiedSinceString == null) {
            return true;
        }
        long ifModifiedSinceMillis = DATE_FORMAT.parse(ifModifiedSinceString).getTime();
        File requestedResource = new File(directory, request.getPath());
        long resourceModifiedMillis = requestedResource.lastModified();

        return ifModifiedSinceMillis < resourceModifiedMillis;
    }
}